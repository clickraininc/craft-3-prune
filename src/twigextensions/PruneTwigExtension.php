<?php
/**
 * Prune plugin for Craft CMS 3.x
 *
 * Remove branches from a structure.
 *
 * @link      https://clickrain.com/about/mark-drzycimski
 * @copyright Copyright (c) 2018 Mark Drzycimski
 */

namespace markdrzy\prune\twigextensions;

use markdrzy\prune\Prune;

use Craft;
use Craft\services\Elements as ElementService;

/**
 * @author    Mark Drzycimski
 * @package   Prune
 * @since     1.0.0
 */
class PruneTwigExtension extends \Twig_Extension
{
    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'Prune';
    }

    /**
     * @inheritdoc
     */
    public function getFilters()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('prune', [$this, 'prune']),
        ];
    }

    /**
     * @param null $text
     *
     * @return array
     */
    public function prune($entries, $entryTypes)
    {
        // Sorting by 'lft' should override any .orderby() sorting
        usort($entries, function($a, $b){
            return $a['lft'] - $b['lft'];
        });

        $output = array();
        $startFilter = 0;
        $stopFilter = 0;

        foreach ($entries as $entry) {
            if (in_array($entry['type'],$entryTypes) && $entry['lft'] > 0 && $entry['lft'] > $stopFilter) {
                $startFilter = $entry['lft'];
                $stopFilter = $entry['rgt'];
            }
            if ($entry['lft'] < $startFilter || $entry['rgt'] > $stopFilter) {
                $output[] = $entry;
            }
        }

        return $output;
    }
}
