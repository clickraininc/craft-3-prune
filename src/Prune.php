<?php
/**
 * Prune plugin for Craft CMS 3.x
 *
 * Remove branches from a structure.
 *
 * @link      https://clickrain.com/about/mark-drzycimski
 * @copyright Copyright (c) 2018 Mark Drzycimski
 */

namespace markdrzy\prune;

use markdrzy\prune\twigextensions\PruneTwigExtension;

use Craft;
use craft\base\Plugin;
use craft\services\Plugins;
use craft\events\PluginEvent;

use yii\base\Event;

/**
 * Class Prune
 *
 * @author    Mark Drzycimski
 * @package   Prune
 * @since     1.0.0
 *
 */
class Prune extends Plugin
{
    // Static Properties
    // =========================================================================

    /**
     * @var Prune
     */
    public static $plugin;

    // Public Properties
    // =========================================================================

    /**
     * @var string
     */
    public $schemaVersion = '1.0.0';

    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        self::$plugin = $this;

        Craft::$app->view->registerTwigExtension(new PruneTwigExtension());

        Event::on(
            Plugins::class,
            Plugins::EVENT_AFTER_INSTALL_PLUGIN,
            function (PluginEvent $event) {
                if ($event->plugin === $this) {
                }
            }
        );

        Craft::info(
            Craft::t(
                'prune',
                '{name} plugin loaded',
                ['name' => $this->name]
            ),
            __METHOD__
        );
    }

    // Protected Methods
    // =========================================================================

}
