<?php
/**
 * Prune plugin for Craft CMS 3.x
 *
 * Remove entire branches from a structure.
 *
 * @link      https://clickrain.com/about/mark-drzycimski
 * @copyright Copyright (c) 2018 Mark Drzycimski
 */

/**
 * @author    Mark Drzycimski
 * @package   Prune
 * @since     1.0.0
 */
return [
    'Prune plugin loaded' => 'Prune plugin loaded',
];
