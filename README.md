# Prune plugin for Craft CMS 3.x

Remove entire branches from a structure.

![Screenshot](resources/img/plugin-logo.png)

## Requirements

This plugin requires Craft CMS 3.

## Installation

To install the plugin, follow these instructions.

1. Open your terminal and go to your Craft project: `cd /path/to/project`

2. Add the following to the "repositories" section of your `composer.json` file:

```
    {
      "type": "vcs",
      "url": "https://bitbucket.org/clickraininc/craft-3-prune.git"
    }
```

1. Then tell Composer to load the plugin: `composer require markdrzy/prune`

2. In the Control Panel, go to Settings → Plugins and click the “Install” button for Prune.

## Prune Overview

Prune allows you to remove branches from a Structure. Create one or more "hidden" Entry Types to exclude an entry and all of the entry's descendants from an entries query. Then pass the results of a query to the `prune` function and provide a list of Entry Types you wish to remove from the structure.

## Using Prune

Example:

```
{% set allPages = craft.entries.section('pages').all() %}
{% set hiddenEntryTypes = ['hiddenPage', 'secretPage'] %}
{% set prunedPages = prune(allPages, hiddenEntryTypes) %}

<ol>
  {% nav page in prunedPages %}
    <li>
      <a href="{{ page.url }}>{{ page.title }}</a>
    </li>
  {% endnav %}
</ol>
```